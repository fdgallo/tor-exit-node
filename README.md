## README (Español):

Este repositorio contiene las instrucciones paso a paso llevadas a cabo por Facundo Gallo-Serpillo para el despliegue del nodo de salida Yupanki en la red Tor.
El trabajo realizado es parte de una investigación académica sobre el consumo de la red Tor para acceder a servicios Web.

Para más información sobre el autor y el artículo resultante consulte la web: www.facundogallo.es

## README (English):
This repository contains Facundo Gallo-Serpillo's step-by-step instructions for deploying the Yupanki exit node on the Tor network.
The work done is part of an academic research on the use of the Tor network to access web services.

More information about the author and the resulting paper can be found at www.facundogallo.es